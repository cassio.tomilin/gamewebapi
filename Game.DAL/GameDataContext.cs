﻿using Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Game.DAL
{
    public class GameDataContext : DbContext
    {
        public GameDataContext()
            : base("GameConnection") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(p => p.Name == "Id")
                .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(x => x.HasMaxLength(200));

            modelBuilder.Properties<string>()
                .Configure(x => x.HasColumnType("varchar"));
        }

        public DbSet<Player> Players { get; set; }
    }
}