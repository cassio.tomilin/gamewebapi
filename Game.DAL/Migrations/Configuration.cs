namespace Game.DAL.Migrations
{
    using DataInitializer;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<GameDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GameDataContext context)
        {
            new PlayerDataInitializer().Initialize(context);
        }
    }
}
