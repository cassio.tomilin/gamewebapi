﻿namespace Game.DAL.Migrations.DataInitializer.Contract
{
    public interface IDataInitializer
    {
        void Initialize(GameDataContext context);
    }
}
