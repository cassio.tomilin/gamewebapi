﻿using Entities;
using Game.DAL.Migrations.DataInitializer.Contract;
using System;
using System.Linq;

namespace Game.DAL.Migrations.DataInitializer
{
    public class PlayerDataInitializer : IDataInitializer
    {
        public void Initialize(GameDataContext context)
        {
            if (!context.Players.Any())
            {
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 1, GameId = 1, Win = 100, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 2, GameId = 1, Win = 150, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 3, GameId = 1, Win = 200, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 4, GameId = 1, Win = 250, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 5, GameId = 1, Win = 300, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 6, GameId = 1, Win = 350, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 7, GameId = 1, Win = 400, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 8, GameId = 1, Win = 450, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 9, GameId = 1, Win = 500, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 10, GameId = 1, Win = 550, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 11, GameId = 1, Win = 600, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 12, GameId = 1, Win = 650, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 13, GameId = 1, Win = 700, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 14, GameId = 1, Win = 750, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 15, GameId = 1, Win = 800, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 16, GameId = 1, Win = 850, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 17, GameId = 1, Win = 900, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 18, GameId = 1, Win = 950, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 19, GameId = 1, Win = 1000, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 20, GameId = 1, Win = 1050, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 21, GameId = 1, Win = 1100, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 22, GameId = 1, Win = 1150, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 23, GameId = 1, Win = 1200, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 24, GameId = 1, Win = 1250, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 25, GameId = 1, Win = 1300, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 26, GameId = 1, Win = 1350, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 27, GameId = 1, Win = 1400, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 28, GameId = 1, Win = 1450, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 29, GameId = 1, Win = 1500, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 30, GameId = 1, Win = 1550, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 31, GameId = 1, Win = 1600, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 32, GameId = 1, Win = 1650, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 33, GameId = 1, Win = 1700, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 34, GameId = 1, Win = 1750, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 35, GameId = 1, Win = 1800, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 36, GameId = 1, Win = 1850, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 37, GameId = 1, Win = 1900, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 38, GameId = 1, Win = 1950, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 39, GameId = 1, Win = 2000, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 40, GameId = 1, Win = 2050, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 41, GameId = 1, Win = 2100, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 42, GameId = 1, Win = 2150, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 43, GameId = 1, Win = 2200, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 44, GameId = 1, Win = 2250, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 45, GameId = 1, Win = 2300, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 46, GameId = 1, Win = 2350, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 47, GameId = 1, Win = 2400, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 48, GameId = 1, Win = 2450, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 49, GameId = 1, Win = 2500, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 50, GameId = 1, Win = 2550, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 51, GameId = 1, Win = 2600, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 52, GameId = 1, Win = 2650, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 53, GameId = 1, Win = 2700, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 54, GameId = 1, Win = 2750, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 55, GameId = 1, Win = 2800, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 56, GameId = 1, Win = 2850, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 57, GameId = 1, Win = 2900, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 58, GameId = 1, Win = 2950, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 59, GameId = 1, Win = 3000, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 60, GameId = 1, Win = 3050, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 61, GameId = 1, Win = 3100, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 62, GameId = 1, Win = 3150, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 63, GameId = 1, Win = 3200, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 64, GameId = 1, Win = 3250, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 65, GameId = 1, Win = 3300, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 66, GameId = 1, Win = 3350, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 67, GameId = 1, Win = 3400, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 68, GameId = 1, Win = 3450, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 69, GameId = 1, Win = 3500, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 70, GameId = 1, Win = 3550, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 71, GameId = 1, Win = 3600, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 72, GameId = 1, Win = 3650, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 73, GameId = 1, Win = 3700, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 74, GameId = 1, Win = 3750, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 75, GameId = 1, Win = 3800, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 76, GameId = 1, Win = 2520, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 77, GameId = 1, Win = 2620, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 78, GameId = 1, Win = 2620, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 79, GameId = 1, Win = 2720, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 80, GameId = 1, Win = 2720, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 81, GameId = 1, Win = 2820, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 82, GameId = 1, Win = 2820, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 83, GameId = 1, Win = 2920, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 84, GameId = 1, Win = 2920, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 85, GameId = 1, Win = 3020, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 86, GameId = 1, Win = 3020, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 87, GameId = 1, Win = 3120, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 88, GameId = 1, Win = 3120, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 89, GameId = 1, Win = 3220, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 90, GameId = 1, Win = 3220, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 91, GameId = 1, Win = 3320, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 92, GameId = 1, Win = 3320, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 93, GameId = 1, Win = 3420, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 94, GameId = 1, Win = 3420, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 95, GameId = 1, Win = 3520, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 96, GameId = 1, Win = 3520, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 97, GameId = 1, Win = 3620, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 98, GameId = 1, Win = 3620, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 99, GameId = 1, Win = 3720, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
                context.Players.Add(new Player() { Id = Guid.NewGuid(), PlayerId = 100, GameId = 1, Win = 3720, Timestamp = DateTime.UtcNow, DataInclusao = DateTime.Now });
  
                context.SaveChanges();
            }
        }
    }
}
