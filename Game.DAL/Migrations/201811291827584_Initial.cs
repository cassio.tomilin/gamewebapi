namespace Game.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Player",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlayerId = c.Long(nullable: false),
                        GameId = c.Long(nullable: false),
                        Win = c.Long(nullable: false),
                        Timestamp = c.DateTime(),
                        DataInclusao = c.DateTime(),
                        DataAlteracao = c.DateTime(),
                        DataExclusao = c.DateTime(),
                        Ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Player");
        }
    }
}
