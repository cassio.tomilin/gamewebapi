namespace Game.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterDatetime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Player", "Timestamp", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Player", "DataInclusao", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Player", "DataInclusao", c => c.DateTime());
            AlterColumn("dbo.Player", "Timestamp", c => c.DateTime());
        }
    }
}
