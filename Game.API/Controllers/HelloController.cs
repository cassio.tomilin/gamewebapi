﻿using Game.DAL;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Game.API.Controllers
{
    /// <summary>
    /// Controller Home
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HelloController : ApiController
    {
        /// <summary>
        /// Método para testar conexão com o SQL.
        /// </summary>
        /// <returns>Retorna a conexão do SQL.</returns>
        [HttpGet]
        public IHttpActionResult Say()
        {

            var testConnectionDB = false;
            string errorTestConnection = string.Empty;
            try
            {
                using (GameDataContext context = new GameDataContext())
                {
                    context.Players.Count();
                }
                testConnectionDB = true;
            }
            catch (Exception ex)
            {
                errorTestConnection = ex.Message;
            }

            var response = new
            {
                applicationIsRunning = true,
                dataBase = new
                {
                    isOnline = testConnectionDB,
                    errorTestConnection = errorTestConnection
                }
            };

            return Ok(response);
        }
    }
}