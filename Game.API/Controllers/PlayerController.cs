﻿using Entities;
using Game.DAL;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace Game.API.Controllers
{
    /// <summary>
    /// Controller do Player
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Player")]
    public class PlayerController : ApiController
    {
        private GameDataContext db = new GameDataContext();

        /// <summary>
        /// Método responsável por resgatar todos os players
        /// </summary>
        /// <remarks>Retorna a lista de players ordenados pela pontuação</remarks>
        /// <response code="500">Internal Server Error.</response>
        [AllowAnonymous]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                using (GameDataContext context = new GameDataContext())
                {
                    return Ok(context.Players.OrderByDescending(x => x.Win).Select(item => new
                    {
                        playerId = item.PlayerId,
                        balance = item.Win,
                        lastUpdateDate = item.Timestamp
                    }).ToList());
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Método responsável por resgatar 100 players (Endpoint 2)
        /// </summary>
        /// <param name="id">Identificador do Game (GameId)</param>
        /// <remarks>Retorna a lista de 100 jogadores ordenados pela pontuação e filtrado pelo Identificador do Game (GameId)</remarks>
        /// <response code="500">Internal Server Error.</response>
        /// <response code="404">Not Found.</response>
        [AllowAnonymous]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                using (GameDataContext context = new GameDataContext())
                {

                    int numberOfrecords = 100;
                    var players = context.Players.OrderByDescending(x => x.Win).Where(x => x.GameId == id).Take(numberOfrecords)
                        .Select(item => new
                        {
                            playerId = item.PlayerId,
                            balance = item.Win,
                            lastUpdateDate = item.Timestamp
                        }).ToList();

                    if (players == null)
                        return NotFound();

                    return Ok(players);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Método responsável por salvar/atualizar os registros dos jogadores. (Endpoint 1)
        /// </summary>
        /// <param name="model">Salvar o jogador</param>
        /// <remarks>Adiciona e edita os registros dos jogadores.</remarks>
        /// <response code="200">Registro Alterado.</response>
        /// <response code="201">Registro Criado.</response>
        /// <response code="500">Internal Server Error.</response>
        [AllowAnonymous]
        [Route("Post")]
        public IHttpActionResult Post(Player model)
        {
            try
            {
                using (GameDataContext context = new GameDataContext())
                {
                    var playerDb = context.Players.FirstOrDefault(x => x.PlayerId == model.PlayerId && x.GameId == model.GameId);
                    var isCreated = false;

                    if (playerDb == null)
                    {
                        model.Id = Guid.NewGuid();
                        playerDb = context.Players.Add(model);
                        isCreated = true;
                        playerDb.DataInclusao = DateTime.Now;
                    }
                    else
                    {
                        playerDb.Win += model.Win;
                        playerDb.DataAlteracao = DateTime.Now;
                        playerDb.Timestamp = DateTime.UtcNow;
                    }

                    context.SaveChanges();

                    if (isCreated)
                        return Created($"{Request.RequestUri.ToString()}/{playerDb.PlayerId}", playerDb);

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}