﻿using System.Web.Http;

namespace Game.API
{
    /// <summary>
    /// WebApiConfig (Configuração da API)
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            // hello route controller
            config.Routes.MapHttpRoute(
                name: "DefaultHelloPage",
                routeTemplate: "{controller}/{action}",
                defaults: new { controller = "hello", action = "say" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.EnableCors();

            //SwaggerConfig.Register();
        }
    }
}
