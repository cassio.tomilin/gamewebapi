﻿using Entities.Base;
using System;

namespace Entities
{
    public class Player : DomainBase
    {
        public long PlayerId { get; set; }

        public long GameId { get; set; }

        public long Win { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
