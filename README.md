# REQUISITOS

Você está trabalhando para uma empresa de jogos online que opera vários servidores de jogos. Cada jogo resulta em ganho ou perda de pontos para o jogador.
Dados são mantidos em memória por cada servidor e periodicamente esses dados são persistidos. Sua tarefa é implementar um serviço que exponha 2 endpoints:

Endpoint 1:

Permite que os servidores persistam os dados do resultado de um jogo:
GameResult:
    playerId (long) – ID do jogador 
    gameId (long) – ID do jogo
    win (long) – o número de pontos ganhos (positivos ou negativos) 
    timestamp (date time) – data de quando o jogo foi realizado(UTC)
Como resultado da chamada a esse endpoint o balanço dos pontos do jogador devem ser, em algum momento, atualizados no banco de dados. Se um jogador não tem um registro do balanço dos seus pontos no banco de dados, ele deverá ser criado. 

NOTA: 
    Um grupo de dados pode conter diversos registros de um único jogador (i.e. o jogador participou em vários jogos). 
    Existem múltiplos servidores de jogos, realizando partidas simultâneas de jogos diferentes, então o serviço irá receber várias requisições concorrentes, que podem conter resultados do mesmo jogador. 
    Inicialmente este serviço irá rodar como um piloto em um único servidor. 
    Portanto, dados perdidos devido ao mal funcionamento do servidor ou do serviço não é considerado crítico, mas não deveria ocorrer dentro de circunstâncias normais.

Endpoint 2:

Esse endpoint permite que os web sites onde o jogador inicia os jogos mostre um placar da classificação dos 100 melhores jogadores. Os 100 melhores jogadores são ordenados pelo balanço de pontos que eles possuem em ordem descendente. Ele retornará os seguintes dados: 
Leaderboard:
    playerId (long) – ID do jogador
    balance (long) – balanço de pontos do jogador
    lastUpdateDate (date time) – data em que o balanço de pontos do jogador foi atualizado pela última vez (usando o fuso horário do servidor de aplicação)


# BANCO DE DADOS

Para criar o banco de dados é necessário executar o comando:
Update-Database -ProjectName Game.DAL -StartUpProjectName Game.API -ConfigurationTypeName Game.DAL.Migrations.Configuration -Verbose 
no Package Manager Console.
Foi criado um seed com 100 registros para testes.

# COMO EXECUTAR

Para executá-lo, é necessário definir a Connection String no Web.Config. Atualmente está definido um banco local.
Após executá-lo irá abrir uma página do chrome que testa se a aplicação está rodando e o banco de dados está funcionando corretemente.
Para realizar os testes (via Swagger) é necessário adicionar /swagger na url da página aberta pelo chrome.

# ESTRUTURA DO PROJETO

Domain:
	Camada de domínio, contendo os modelos (Entities).

Infrastructure:
	Responsável pela conexão com o banco de dados + Migrations e Seed.

Service:
	WebApi responsável por responder os Endpoints solicitados.
